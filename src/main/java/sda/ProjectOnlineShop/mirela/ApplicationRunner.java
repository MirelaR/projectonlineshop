package sda.ProjectOnlineShop.mirela;

import sda.ProjectOnlineShop.mirela.model.Product;
import sda.ProjectOnlineShop.mirela.service.ProductService;

import java.util.List;

public class ApplicationRunner {

    public static void main(String[] arg){
   // 1.Cream o dependinta catre ProductService
        ProductService productService=new ProductService();

        //1.create a new product add it into DB:
        productService.insertNewProduct("Biscuiti",8.7f);

        //2.read all products:
            // A read product by name
        List<Product> products =productService.getAllProducts();

        System.out.println("The products in our shop are:" );
        for (Product currentProduct :products) {
            System.out.println(currentProduct);
        }

        //3.update product by name-find product by name and add new price

        //4.delete product by name-find by name and deleted


    }
}
