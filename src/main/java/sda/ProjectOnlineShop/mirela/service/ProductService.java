package sda.ProjectOnlineShop.mirela.service;

import sda.ProjectOnlineShop.mirela.dao.ProductDao;
import sda.ProjectOnlineShop.mirela.model.Product;

import java.util.List;

public class ProductService {
    //1.cream o variabila globala ce o vom folosi in dao
    ProductDao productDao=new ProductDao();

    public void insertNewProduct(String productName, float price) {
        Product product=new Product();//cream product
        product.setProductName(productName);
        product.setPrice(price);

        productDao.insertProductToDB(product);
    }

    public List<Product> getAllProducts() {
        return productDao.getAllProducts();

    }
}
